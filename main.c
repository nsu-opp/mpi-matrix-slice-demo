#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <mpi/mpi.h>

typedef struct Mat Mat;
struct Mat {
    float *Data;
    int Rows;
    int Cols;
};

static Mat Mat_New(int rows, int cols) {
    float *data = calloc(rows * cols, sizeof(float));
    assert(NULL != data);

    return (Mat) {
            .Data = data,
            .Rows = rows,
            .Cols = cols
    };
}

static void Mat_Print(Mat mat) {
    for (int row = 0; row < mat.Rows; row++) {
        for (int col = 0; col < mat.Cols; col++) {
            printf("%.2f ", mat.Data[row * mat.Cols + col]);
        }
        printf("\n");
    }
}

static void Mat_Free(Mat mat[static 1]) {
    free(mat->Data);
    *mat = (Mat) {0};
}

static MPI_Datatype CreateColumnType(int rows, int cols) {
    MPI_Datatype type;
    MPI_Type_vector(rows, 1, cols, MPI_FLOAT, &type);

    MPI_Datatype typeResized;
    MPI_Type_create_resized(type, 0, sizeof(float), &typeResized);
    MPI_Type_commit(&typeResized);
    MPI_Type_free(&type);

    return typeResized;
}

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int const rowsTotal = 2;
    int const colsTotal = 7;

    int *counts = calloc(size, sizeof(int));
    assert(NULL != counts);

    for (int i = 0; i < size; i++) {
        counts[i] = colsTotal / size;
    }
    if (0 != colsTotal % size) {
        counts[0] += colsTotal % size;
    }

    int *displs = calloc(size, sizeof(int));
    assert(NULL != displs);
    for (int i = 1; i < size; i++) {
        displs[i] = displs[i - 1] + counts[i - 1];
    }

    Mat src = (Mat) {0};
    if (0 == rank) {
        src = Mat_New(rowsTotal, colsTotal);
        for (int i = 0; i < src.Rows * src.Cols; i++) {
            src.Data[i] = (float) i;
        }
        Mat_Print(src);
    }
    Mat dst = Mat_New(rowsTotal, counts[rank]);

    MPI_Datatype sendType = CreateColumnType(src.Rows, src.Cols);
    MPI_Datatype recvType = CreateColumnType(dst.Rows, dst.Cols);

    MPI_Scatterv(
            src.Data, counts, displs, sendType,
            dst.Data, dst.Cols, recvType,
            0, MPI_COMM_WORLD
    );

    for (int i = 0; i < size; i++) {
        MPI_Barrier(MPI_COMM_WORLD);
        sleep(1);
        if (i == rank) {
            printf("rank = %d\n", rank);
            Mat_Print(dst);
        }
    }

    Mat_Free(&src);
    Mat_Free(&dst);
    free(counts);
    free(displs);
    MPI_Type_free(&sendType);
    MPI_Type_free(&recvType);
    MPI_Finalize();
    return EXIT_SUCCESS;
}
